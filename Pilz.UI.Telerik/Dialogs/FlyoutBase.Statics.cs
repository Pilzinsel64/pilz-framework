﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls.UI.SplashScreen;
using Telerik.WinControls.UI;
using System.Windows.Forms;

namespace Pilz.UI.Telerik.Dialogs
{
    partial class FlyoutBase
    {
        public delegate void FlyoutCreatedEventHandler(FlyoutCreatedEventArgs e);
        public delegate void FlyoutClosedEventHandler(FlyoutClosedEventArgs e);

        public static event FlyoutCreatedEventHandler FlyoutCreated
        {
            add => flyoutCreatedHandlers.Add(value);
            remove => flyoutCreatedHandlers.Remove(value);
        }

        public static event FlyoutClosedEventHandler FlyoutClosed
        {
            add => flyoutCloseHandlers.Add(value);
            remove => flyoutCloseHandlers.Remove(value);
        }

        private static readonly List<FlyoutCreatedEventHandler> flyoutCreatedHandlers = new();
        private static readonly List<FlyoutClosedEventHandler> flyoutCloseHandlers = new();
        
        private static object? tagToAssign  = null;
        public static Control? ParentContext { get; private set; } = null;

        static FlyoutBase()
        {
            RadFlyoutManager.ContentCreated += RadFlyoutManager_ContentCreated;
            RadFlyoutManager.FlyoutClosed += RadFlyoutManager_FlyoutClosed;
        }

        private static void RadFlyoutManager_ContentCreated(ContentCreatedEventArgs e)
        {
            if (e.Content is FlyoutBase dialogBase)
            {
                if (tagToAssign != null)
                    dialogBase.Tag = tagToAssign;

                var eventArgs = new FlyoutCreatedEventArgs((FlyoutBase)e.Content);

                if (dialogBase is ILoadContent iLoadContent)
                    iLoadContent.LoadContent();

                foreach (var args in flyoutCreatedHandlers)
                {
                    if (ParentContext != null)
                        ParentContext?.Invoke(args, eventArgs);
                    else
                        args.Invoke(eventArgs);
                }
            }
        }

        private static void RadFlyoutManager_FlyoutClosed(global::Telerik.WinControls.UI.SplashScreen.FlyoutClosedEventArgs e)
        {
            if (e.Content is FlyoutBase dialogBase)
            {
                var eventArgs = new FlyoutClosedEventArgs((FlyoutBase)e.Content);

                foreach (var args in flyoutCloseHandlers)
                {
                    if (ParentContext != null)
                        ParentContext?.Invoke(args, eventArgs);
                    else
                        args.Invoke(eventArgs);
                }
            }

            ParentContext = null;
        }

        public static void Show<T>(Control controlToAssociate, object? tag = null)
        {
            Show(controlToAssociate, typeof(T), tag);
        }

        public static void Show(Control controlToAssociate, Type flyoutContentType, object? tag = null)
        {
            tagToAssign = tag;
            ParentContext = controlToAssociate;
            RadFlyoutManager.Show(controlToAssociate, flyoutContentType);
        }

        protected static void CloseFlyout()
        {
            if (ParentContext == null)
                throw new NullReferenceException(nameof(ParentContext));

            ParentContext.BeginInvoke(RadFlyoutManager.Close);
        }
    }
}
