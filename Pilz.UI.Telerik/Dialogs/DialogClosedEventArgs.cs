﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pilz.UI.Telerik.Dialogs
{
    public class DialogClosedEventArgs : EventArgs
    {
        public DialogBase Parent { get; private set; }
        public FlyoutBase? Content => Parent?.DialogPanel;

        public DialogClosedEventArgs(DialogBase dialog)
        {
            Parent = dialog;
        }
    }
}
