﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI.SplashScreen;

namespace Pilz.UI.Telerik.Dialogs
{
    public class FlyoutCreatedEventArgs : ContentCreatedEventArgs
    {
        public new FlyoutBase? Content => base.Content as FlyoutBase;

        public FlyoutCreatedEventArgs(FlyoutBase content) : base(content)
        {
        }
    }
}
