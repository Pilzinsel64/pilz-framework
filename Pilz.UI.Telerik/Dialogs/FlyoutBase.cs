﻿using Pilz.UI.Telerik;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.SplashScreen;
using static Telerik.WinControls.UI.PopupEditorNotificationData;

namespace Pilz.UI.Telerik.Dialogs
{
    public partial class FlyoutBase : UserControl
    {
        public static RadSvgImage? CancelSvg { get; set; } = null;
        public static RadSvgImage? ConfirmSvg { get; set; } = null;

        public DialogResult Result { get; protected set; }
        public bool RegisterDialogAccept { get; set; } = true;
        public bool RegisterDialogCancel { get; set; } = false;

        protected bool ActionPanelVisible
        {
            get => tableLayoutPanel_ActionButtons.Visible;
            set => tableLayoutPanel_ActionButtons.Visible = value;
        }

        protected bool CancelButtonVisible
        {
            get => radButton_Cancel.Visible;
            set => radButton_Cancel.Visible = value;
        }

        protected bool CancelButtonEnable
        {
            get => radButton_Cancel.Enabled;
            set => radButton_Cancel.Enabled = value;
        }

        protected bool ConfirmButtonEnable
        {
            get => radButton_Confirm.Enabled;
            set => radButton_Confirm.Enabled = value;
        }

        protected FlyoutBase()
        {
            InitializeComponent();
            ParentChanged += FlyoutDialogBase_ParentChanged;

            // SVG Symbols
            radButton_Cancel.SvgImage = CancelSvg;
            radButton_Confirm.SvgImage = ConfirmSvg;
        }

        private void FlyoutDialogBase_ParentChanged(object? sender, EventArgs e)
        {
            var frm = FindForm();
            if (frm != null)
            {
                if (RegisterDialogAccept)
                    frm.AcceptButton = radButton_Confirm;
                if (RegisterDialogCancel)
                    frm.CancelButton = radButton_Cancel;
            }
        }

        protected void Close(DialogResult result)
        {
            Result = result;

            if (FindForm() is DialogBase dialogForm)
                dialogForm.Close();
            else
                CloseFlyout();
        }

        private void RadButton_Confirm_Click(object sender, EventArgs e)
        {
            if (ValidateOK())
                Close(DialogResult.OK);
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            Close(DialogResult.Cancel);
        }

        protected virtual bool ValidateOK()
        {
            return true;
        }
    }
}
