﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pilz.UI.Telerik.Dialogs
{
    public class FlyoutClosedEventArgs : global::Telerik.WinControls.UI.SplashScreen.FlyoutClosedEventArgs
    {
        public new FlyoutBase? Content => base.Content as FlyoutBase;

        public FlyoutClosedEventArgs(FlyoutBase content) : base(content)
        {
        }
    }
}
