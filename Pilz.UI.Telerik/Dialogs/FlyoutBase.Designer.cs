﻿namespace Pilz.UI.Telerik.Dialogs
{
    partial class FlyoutBase
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlyoutBase));
            radButton_Cancel = new global::Telerik.WinControls.UI.RadButton();
            radButton_Confirm = new global::Telerik.WinControls.UI.RadButton();
            tableLayoutPanel_ActionButtons = new TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)radButton_Cancel).BeginInit();
            ((System.ComponentModel.ISupportInitialize)radButton_Confirm).BeginInit();
            tableLayoutPanel_ActionButtons.SuspendLayout();
            SuspendLayout();
            // 
            // radButton_Cancel
            // 
            resources.ApplyResources(radButton_Cancel, "radButton_Cancel");
            radButton_Cancel.Name = "radButton_Cancel";
            radButton_Cancel.Click += RadButton_Cancel_Click;
            // 
            // radButton_Confirm
            // 
            resources.ApplyResources(radButton_Confirm, "radButton_Confirm");
            radButton_Confirm.Name = "radButton_Confirm";
            radButton_Confirm.Click += RadButton_Confirm_Click;
            // 
            // tableLayoutPanel_ActionButtons
            // 
            resources.ApplyResources(tableLayoutPanel_ActionButtons, "tableLayoutPanel_ActionButtons");
            tableLayoutPanel_ActionButtons.Controls.Add(radButton_Confirm, 1, 0);
            tableLayoutPanel_ActionButtons.Controls.Add(radButton_Cancel, 2, 0);
            tableLayoutPanel_ActionButtons.Name = "tableLayoutPanel_ActionButtons";
            // 
            // FlyoutDialogBase
            // 
            resources.ApplyResources(this, "$this");
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(tableLayoutPanel_ActionButtons);
            Name = "FlyoutDialogBase";
            ((System.ComponentModel.ISupportInitialize)radButton_Cancel).EndInit();
            ((System.ComponentModel.ISupportInitialize)radButton_Confirm).EndInit();
            tableLayoutPanel_ActionButtons.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private global::Telerik.WinControls.UI.RadButton radButton_Cancel;
        private global::Telerik.WinControls.UI.RadButton radButton_Confirm;
        private TableLayoutPanel tableLayoutPanel_ActionButtons;
    }
}
