﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Pilz.UI.Telerik.Dialogs
{
    public partial class DialogBase : RadForm
    {
        public FlyoutBase? DialogPanel { get; private set; }

        private DialogBase()
        {
            Load += DialogBaseForm_Load;
            FormClosed += DialogBaseForm_FormClosed;
        }

        public DialogBase(FlyoutBase? dialogPanel) : this()
        {
            DialogPanel = dialogPanel;
        }

        private void DialogBaseForm_Load(object? sender, EventArgs e)
        {
            if (DialogPanel is ILoadContent iLoadContent)
                iLoadContent.LoadContent();

            DialogLoading?.Invoke(new DialogLoadingEventArgs(this));
        }

        private void DialogBaseForm_FormClosed(object? sender, FormClosedEventArgs e)
        {
            DialogClosed?.Invoke(new DialogClosedEventArgs(this));
        }
    }
}
