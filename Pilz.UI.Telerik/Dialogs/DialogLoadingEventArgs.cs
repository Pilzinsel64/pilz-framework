﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Pilz.UI.Telerik.Dialogs
{
    public class DialogLoadingEventArgs : EventArgs
    {
        public DialogBase Parent { get; private set; }
        public FlyoutBase? Content => Parent?.DialogPanel;

        public DialogLoadingEventArgs(DialogBase dialog)
        {
            Parent = dialog;
        }
    }
}
