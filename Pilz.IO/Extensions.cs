﻿using Pilz.Runtime;
using Pilz.Win32.Native;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Pilz.IO
{
    public static class Extensions
    {
        static readonly int MAX_PATH = 255;

        public static string GetExecutablePath(bool checkRealOS = false)
        {
            if (RuntimeInformationsEx.IsOSPlatform(OSType.Windows, checkRealOS))
            {
                var sb = new StringBuilder(MAX_PATH);
                Kernel32.GetModuleFileName(IntPtr.Zero, sb, MAX_PATH);
                return sb.ToString();
            }
            else
                return Process.GetCurrentProcess().MainModule.FileName;
        }
    }
}
