﻿using Newtonsoft.Json;
using Pilz.Networking.CloudProviders.Nextcloud.Client.LoginFlowV2.Ocs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud
{
    public class NextcloudLogin
    {
        /// <summary>
        /// The server url the login credentials are for.
        /// </summary>
        public string? Server { get; set; }

        /// <summary>
        /// The login name (username or password) used for the login.
        /// </summary>
        public string? LoginName { get; set; }

        /// <summary>
        /// The app password that has been generated.
        /// </summary>
        public string? AppPassword { get; set; }

        public NextcloudLogin()
        {
        }

        public NextcloudLogin(OcsResponseLoginFlowV2Credentials response)
        {
            Server = response.Server;
            LoginName = response.LoginName;
            AppPassword = response.AppPassword;
        }
    }
}
