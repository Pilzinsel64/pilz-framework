﻿using Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.Tables.Model;
using Pilz.Networking.CloudProviders.Nextcloud.Ocs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.Tables
{
    public class OcsApiTables : OcsApiBase
    {
        public static readonly OcsApiUrlPath OCS_TABLES_TABLE_ROWS = new("/apps/tables/api/1/tables/{0}/rows");
        public static readonly OcsApiUrlPath OCS_TABLES_VIEW_ROWS = new("/apps/tables/api/1/views/{0}/rows");
        public static readonly OcsApiUrlPath OCS_TABLES_TABLE_ROWS_SIMPLE = new("/apps/tables/api/1/tables/{0}/rows/simple");
        public static readonly OcsApiUrlPath OCS_TABLES_TABLE_COLUMNS = new("/apps/tables/api/1/tables/{0}/columns");
        public static readonly OcsApiUrlPath OCS_TABLES_VIEW_COLUMNS = new("/apps/tables/api/1/views/{0}/columns");
        public static readonly OcsApiUrlPath OCS_TABLES_ROW = new("/apps/tables/api/1/rows/{0}");
        public static readonly OcsApiUrlPath OCS_TABLES_COLUMN = new("/apps/tables/api/1/column/{0}");

        public OcsApiTables(OcsApi manager) : base(manager)
        {
        }

        public RowsSimple? GetRowsSimple(long tableId)
        {
            return Manager.MakeRequest<RowsSimple>(HttpMethod.Get, OCS_TABLES_TABLE_ROWS_SIMPLE.FillParameters(tableId));
        }

        public Rows? GetRows(long tableId)
        {
            return Manager.MakeRequest<Rows>(HttpMethod.Get, OCS_TABLES_TABLE_ROWS.FillParameters(tableId));
        }

        public Rows? GetViewRows(long viewId)
        {
            return Manager.MakeRequest<Rows>(HttpMethod.Get, OCS_TABLES_VIEW_ROWS.FillParameters(viewId));
        }

        public Row? GetRow(long rowId)
        {
            return Manager.MakeRequest<Row>(HttpMethod.Get, OCS_TABLES_ROW.FillParameters(rowId));
        }

        public Columns? GetColumns(long tableId)
        {
            return Manager.MakeRequest<Columns>(HttpMethod.Get, OCS_TABLES_TABLE_COLUMNS.FillParameters(tableId));
        }

        public Columns? GetViewColumns(long viewId)
        {
            return Manager.MakeRequest<Columns>(HttpMethod.Get, OCS_TABLES_VIEW_COLUMNS.FillParameters(viewId));
        }

        public Column? GetColumn(long columnId)
        {
            return Manager.MakeRequest<Column>(HttpMethod.Get, OCS_TABLES_COLUMN.FillParameters(columnId));
        }

        public Row? DeleteRow(long rowId)
        {
            return Manager.MakeRequest<Row>(HttpMethod.Delete, OCS_TABLES_ROW.FillParameters(rowId));
        }

        public Column? DeleteColumn(long columnId)
        {
            return Manager.MakeRequest<Column>(HttpMethod.Delete, OCS_TABLES_COLUMN.FillParameters(columnId));
        }

        public Row? UpdateRow(long rowId, RowUpdate values)
        {
            return Manager.MakeRequest<Row>(HttpMethod.Put, OCS_TABLES_ROW.FillParameters(rowId), content: values);
        }

        public Row? CreateRow(long tableId, RowUpdate values)
        {
            return Manager.MakeRequest<Row>(HttpMethod.Post, OCS_TABLES_TABLE_ROWS.FillParameters(tableId), content: values);
        }

        public Row? CreateViewRow(long viewId, RowUpdate values)
        {
            return Manager.MakeRequest<Row>(HttpMethod.Post, OCS_TABLES_VIEW_ROWS.FillParameters(viewId), content: values);
        }
    }
}
