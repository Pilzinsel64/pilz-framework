﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.Tables.Model
{
    public class Column
    {
        [JsonProperty("type")]
        private string? type;

        [JsonProperty("subtype")]
        private string? subtype;

        [JsonProperty("id")]
        public long ColumnId { get; set; }

        [JsonProperty("tableId")]
        public long TableId { get; set; }

        [JsonProperty("title")]
        public string? Title { get; set; }

        [JsonProperty("createdBy")]
        public string? CreatedBy { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("lastEditBy")]
        public string? LastEditBy { get; set; }

        [JsonProperty("lastEditAt")]
        public DateTime LastEditAt { get; set; }

        [JsonProperty("mandatory")]
        public bool Mandatory { get; set; }

        [JsonProperty("description")]
        public string? Description { get; set; }

        [JsonProperty("numberDefault")]
        public float? NumberDefault { get; set; }

        [JsonProperty("numberMin")]
        public float? NumberMin { get; set; }

        [JsonProperty("numberMax")]
        public float? NumberMax { get; set; }

        [JsonProperty("numberDecimals")]
        public float NumberDecimals { get; set; }

        [JsonProperty("numberPrefix")]
        public string? NumberPrefix { get; set; }

        [JsonProperty("numberSuffix")]
        public string? NumberSuffix { get; set; }

        [JsonProperty("textDefault")]
        public string? TextDefault { get; set; }

        [JsonProperty("textAllowedPattern")]
        public string? TextAllowedPattern { get; set; }

        [JsonProperty("textMaxLength")]
        public int? TextMaxLength { get; set; }

        [JsonProperty("selectionOptions")]
        public List<ColumnSelectionOption> SelectionOptions { get; } = new();

        [JsonProperty("selectionDefault")]
        public int? SelectionDefault { get; set; }

        [JsonProperty("datetimeDefault")]
        public DateTime? DatetimeDefault { get; set; }

        [JsonIgnore]
        public ColumnType Type
        {
            get => type switch
            {
                "text" => ColumnType.Text,
                "selection" => ColumnType.Selection,
                "datetime" => ColumnType.DateTime,
                _ => ColumnType.Unknown,
            };
            set => type = value switch
            {
                ColumnType.Text => "text",
                ColumnType.Selection => "selection",
                ColumnType.DateTime => "datetime",
                _ => "text"
            };
        }

        [JsonIgnore]
        public ColumnSubtype Subtype
        {
            get => subtype switch
            {
                "line" => ColumnSubtype.Line,
                "" => ColumnSubtype.None,
                _ => ColumnSubtype.Unknown,
            };
            set => subtype = value switch
            {
                ColumnSubtype.Line => "line",
                _ => ""
            };
        }
    }
}
