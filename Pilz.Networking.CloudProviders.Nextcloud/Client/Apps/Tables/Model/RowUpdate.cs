﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.Tables.Model
{
    public class RowUpdate
    {
        [JsonProperty("data")]
        public Dictionary<long, object?> Data { get; set; } = new();
    }
}
