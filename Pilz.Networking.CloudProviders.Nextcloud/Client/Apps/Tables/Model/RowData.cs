﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.Tables.Model
{
    public class RowData
    {
        [JsonProperty("columnId")]
        public long ColumnId { get; set; }

        [JsonProperty("value")]
        public object? Value { get; set; }
    }
}
