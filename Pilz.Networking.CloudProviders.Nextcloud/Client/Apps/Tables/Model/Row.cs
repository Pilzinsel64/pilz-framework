﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.Tables.Model
{
    public class Row
    {
        [JsonProperty("id")]
        public long RowId { get; set; } = -1;

        [JsonProperty("tableId")]
        public long TableId { get; set; } = -1;

        [JsonProperty("createdBy")]
        public string? CreatedBy { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("lastEditBy")]
        public string? LastEditBy { get; set; }

        [JsonProperty("lastEditAt")]
        public DateTime LastEditAt { get; set; }

        [JsonProperty("data")]
        public List<RowData> Data { get; set; } = new();
    }
}
