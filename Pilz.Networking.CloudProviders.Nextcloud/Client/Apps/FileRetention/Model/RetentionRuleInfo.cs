﻿using Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.FileRetention.Ocs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.FileRetention.Model
{
    public class RetentionRuleInfo
    {
        /// <summary>
        /// The ID for the tag that is used for this rule.
        /// </summary>
        public int TagID { get; init; }

        /// <summary>
        /// The unit used for the time.
        /// </summary>
        public RetentionTimeUnit TimeUnit { get; init; }

        /// <summary>
        /// Represents numer of days/weeks/months/years.
        /// </summary>
        public int TimeAmount { get; init; }

        /// <summary>
        /// The time used for the rule.
        /// </summary>
        public RetentionTimeAfter TimeAfter { get; init; }

        public OcsDataRetentionRule ToOcsData()
        {
            return new OcsDataRetentionRule
            {
                TagID = TagID,
                TimeUnit = (int)TimeUnit,
                TimeAmount = TimeAmount,
                TimeAfter = (int)TimeAfter
            };
        }
    }
}
