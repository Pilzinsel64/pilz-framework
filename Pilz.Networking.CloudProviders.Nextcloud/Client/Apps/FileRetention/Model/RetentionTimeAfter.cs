﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.FileRetention.Model
{
    public enum RetentionTimeAfter
    {
        CreationDate,
        LastAccess
    }
}
