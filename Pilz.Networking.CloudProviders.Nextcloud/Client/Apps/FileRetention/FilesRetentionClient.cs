﻿using Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.FileRetention.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.FileRetention
{
    public class FilesRetentionClient : ClientBase
    {
        public FilesRetentionClient(NextcloudClient client) : base(client)
        {
        }

        public bool CreateRetentionRule(RetentionRuleInfo rule)
        {
            var entry = rule.ToOcsData();
            return Client.Ocs.GetApi<OcsApiFilesRetention>().CreateRetentionRule(entry);
        }

        public bool DeleteRetentionRule(int ruleID)
        {
            return Client.Ocs.GetApi<OcsApiFilesRetention>().DeleteRetentionRule(ruleID);
        }

        public RetentionRule[]? GetRetentionRules()
        {
            var api = Client.Ocs.GetApi<OcsApiFilesRetention>();
            var response = api.GetRetentionRules();

            if (response?.Data is not null)
            {
                var rules = new List<RetentionRule>();

                foreach (var entry in response.Data)
                    rules.Add(new RetentionRule(entry));

                return rules.ToArray();
            }

            return null;
        }
    }
}
