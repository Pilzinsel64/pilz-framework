﻿using Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.FileRetention.Ocs;
using Pilz.Networking.CloudProviders.Nextcloud.Ocs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.FileRetention
{
    public class OcsApiFilesRetention : OcsApiBase
    {
        public static readonly OcsApiUrlPath OCS_FILE_RETENTION_RULES = new("/ocs/v2.php/apps/files_retention/api/v1/retentions");
        public static readonly OcsApiUrlPath OCS_FILE_RETENTION_RULE = new("/ocs/v2.php/apps/files_retention/api/v1/retentions/{0}");

        public OcsApiFilesRetention(OcsApi manager) : base(manager)
        {
        }

        public bool CreateRetentionRule(OcsDataRetentionRule rule)
        {
            var response = Manager.MakeRequest(HttpMethod.Post, OCS_FILE_RETENTION_RULES, content: rule);
            return response.IsSuccessStatusCode;
        }

        public bool DeleteRetentionRule(int ruleID)
        {
            var response = Manager.MakeRequest(HttpMethod.Delete, OCS_FILE_RETENTION_RULE.FillParameters(ruleID));
            return response.IsSuccessStatusCode;
        }

        public OcsResponseRetention? GetRetentionRules()
        {
            return Manager.MakeRequestOcs<OcsResponseRetention>(HttpMethod.Get, OCS_FILE_RETENTION_RULES);
        }
    }
}
