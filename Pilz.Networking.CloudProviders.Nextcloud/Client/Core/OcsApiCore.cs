﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pilz.Networking.CloudProviders.Nextcloud.Ocs;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Core
{
    public class OcsApiCore : OcsApiBase
    {
        public static readonly OcsApiUrlPath OCS_CORE_APPPASSWORD = "/ocs/v2.php/core/apppassword";

        public OcsApiCore(OcsApi manager) : base(manager)
        {
        }

        public bool DeleteAppPassword()
        {
            using var msg = Manager.MakeRequest(HttpMethod.Delete, OCS_CORE_APPPASSWORD);
            return msg != null && msg.IsSuccessStatusCode;
        }
    }
}
