﻿using Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.Tables;
using Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.Tables.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client
{
    public abstract class ClientBase
    {
        protected NextcloudClient Client { get; init; }

        protected ClientBase(NextcloudClient client)
        {
            Client = client;
        }
    }
}
