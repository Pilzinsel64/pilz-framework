﻿using Pilz.Networking.CloudProviders.Nextcloud.Client.Cloud.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Cloud
{
    public class CloudClient : ClientBase
    {
        public CloudClient(NextcloudClient client) : base(client)
        {
        }

        public UserInfo? GetUserInfo()
        {
            if (!string.IsNullOrEmpty(Client.CurrentLogin?.LoginName))
                return GetUserInfo(Client.CurrentLogin.LoginName);
            else
                return null;
        }

        public UserInfo? GetUserInfo(string username)
        {
            var result = Client.Ocs.Cloud.GetUserMeta(username);

            if (result?.Data != null)
                return new UserInfo(result.Data);

            return null;
        }
    }
}
