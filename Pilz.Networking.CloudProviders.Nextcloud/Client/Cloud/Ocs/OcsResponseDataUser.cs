﻿using Newtonsoft.Json;
using Pilz.Networking.CloudProviders.Nextcloud.Ocs.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Cloud.Ocs
{
    public class OcsResponseDataUser : OcsResponseData
    {
        public class ResponseQuota
        {
            [JsonProperty("free")]
            public long? Free { get; set; }

            [JsonProperty("used")]
            public long? Used { get; set; }

            [JsonProperty("total")]
            public long? Total { get; set; }

            [JsonProperty("relative")]
            public float? Relative { get; set; }

            [JsonProperty("quota")]
            public long? Quota { get; set; }
        }

        public class ResponseBackendCapabilities
        {
            [JsonProperty("setDisplayName")]
            public bool? SetDisplayName { get; set; }

            [JsonProperty("setPassword")]
            public bool? SetPassword { get; set; }
        }

        [JsonProperty("enabled")]
        public bool? Enabled { get; set; }

        [JsonProperty("storageLocation")]
        public string? StorageLocation { get; set; }

        [JsonProperty("id")]
        public string? ID { get; set; }

        [JsonProperty("lastLogin")]
        public long? LastLogin { get; set; }

        [JsonProperty("backend")]
        public string? Backend { get; set; }

        [JsonProperty("quota")]
        public ResponseQuota? Quota { get; set; }

        [JsonProperty("email")]
        public string? Email { get; set; }

        [JsonProperty("displayname")]
        public string? Displayname { get; set; }

        [JsonProperty("display-name")]
        public string? Displayname2 { get; set; }

        [JsonProperty("phone")]
        public string? Phone { get; set; }

        [JsonProperty("address")]
        public string? Address { get; set; }

        [JsonProperty("website")]
        public string? Website { get; set; }

        [JsonProperty("twitter")]
        public string? Twitter { get; set; }

        [JsonProperty("groups")]
        public string[]? Groups { get; set; }

        [JsonProperty("language")]
        public string? Language { get; set; }

        [JsonProperty("locale")]
        public string? Locale { get; set; }

        [JsonProperty("backendCapabilities")]
        public ResponseBackendCapabilities? BackendCapabilities { get; set; }
    }
}
