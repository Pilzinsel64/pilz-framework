﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Cloud.Model
{
    public class UserBackendCapabilities
    {
        /// <summary>
        /// Defines if the display name can be changed.
        /// </summary>
        public bool SetDisplayName { get; set; }

        /// <summary>
        /// Defines if the password can be changed.
        /// </summary>
        public bool SetPassword { get; set; }
    }
}
