﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Cloud.Model
{
    public class UserQuota
    {
        /// <summary>
        /// Amount of free bytes left.
        /// </summary>
        public long Free { get; set; }

        /// <summary>
        /// Amount of already used bytes.
        /// </summary>
        public long Used { get; set; }

        /// <summary>
        /// Total amount of all bytes (free + used).
        /// </summary>
        public long Total { get; set; }

        /// <summary>
        /// Relative amount of used quota in percent.
        /// </summary>
        public float Relative { get; set; }

        /// <summary>
        /// Total amount of bytes available.
        /// </summary>
        public long Quota { get; set; }
    }
}
