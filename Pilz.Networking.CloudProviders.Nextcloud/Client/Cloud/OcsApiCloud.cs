﻿using Pilz.Networking.CloudProviders.Nextcloud.Client.Cloud.Ocs;
using Pilz.Networking.CloudProviders.Nextcloud.Ocs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.Cloud
{
    public class OcsApiCloud : OcsApiBase
    {
        public readonly static OcsApiUrlPath OCS_CLOUD_USER_METADATA = new("/ocs/v1.php/cloud/users/{0}");

        public OcsApiCloud(OcsApi manager) : base(manager)
        {
        }

        public OcsResponseUser? GetUserMeta(string username)
        {
            return Manager.MakeRequestOcs<OcsResponseUser>(HttpMethod.Get, OCS_CLOUD_USER_METADATA.FillParameters(username));
        }
    }
}
