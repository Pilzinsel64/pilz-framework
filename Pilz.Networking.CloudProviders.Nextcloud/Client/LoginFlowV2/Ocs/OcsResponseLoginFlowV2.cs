﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.LoginFlowV2.Ocs
{
    public class OcsResponseLoginFlowV2
    {
        public class PollData
        {
            /// <summary>
            /// The login token that has been created for the login process.
            /// It can be used to poll the login state.
            /// </summary>
            [JsonProperty("token")]
            public string? Token { get; set; }

            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("endpoint")]
            public string? Endpoint { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("poll")]
        public PollData? Poll { get; set; }

        /// <summary>
        /// The temporary login url that should be used for login.
        /// </summary>
        [JsonProperty("login")]
        public string? LoginUrl { get; set; }
    }
}
