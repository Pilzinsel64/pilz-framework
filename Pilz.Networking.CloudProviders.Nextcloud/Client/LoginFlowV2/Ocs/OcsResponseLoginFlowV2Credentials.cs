﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Client.LoginFlowV2.Ocs
{
    public class OcsResponseLoginFlowV2Credentials
    {
        /// <summary>
        /// The server url the login credentials are for.
        /// </summary>
        [JsonProperty("server")]
        public string? Server { get; set; }

        /// <summary>
        /// The login name (username or password) used for the login.
        /// </summary>
        [JsonProperty("loginName")]
        public string? LoginName { get; set; }

        /// <summary>
        /// The app password that has been generated.
        /// </summary>
        [JsonProperty("appPassword")]
        public string? AppPassword { get; set; }
    }
}
