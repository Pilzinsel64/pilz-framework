﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Ocs
{
    public abstract class OcsApiBase
    {
        protected OcsApi Manager { get; init; }

        protected OcsApiBase(OcsApi manager)
        {
            Manager = manager;
        }
    }
}
