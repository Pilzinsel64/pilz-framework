﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Ocs
{
    public delegate void GetOcsApiAuthCredentailsEventHandler(object sender, GetOcsApiAuthCredentailsEventArgs eventArgs);

    public class GetOcsApiAuthCredentailsEventArgs : EventArgs
    {
        public OcsApiAuthCredentials? Credentials { get; set; }
    }
}
