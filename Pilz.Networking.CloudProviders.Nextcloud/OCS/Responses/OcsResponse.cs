﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Ocs.Responses
{
    public class OcsResponse<TMeta, TData> : IOcsResponse where TMeta : IOcsResponseMeta where TData : IOcsResponseData
    {
        [JsonProperty("meta")]
        public TMeta? Meta { get; set; }

        [JsonProperty("data")]
        public TData? Data { get; set; }
    }

    public class OcsResponse<TData> : OcsResponse<OcsResponseMeta, TData> where TData : IOcsResponseData
    {
    }

    public class OcsResponse : OcsResponse<OcsResponseMeta, OcsResponseData>
    {
    }
}
