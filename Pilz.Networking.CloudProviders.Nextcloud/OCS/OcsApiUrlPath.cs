﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Ocs
{
    public readonly struct OcsApiUrlPath
    {
        private readonly string path;

        public OcsApiUrlPath()
        {
            path = string.Empty;
        }

        public OcsApiUrlPath(string path)
        {
            this.path = path;
        }

        public static implicit operator string(OcsApiUrlPath o) => o.path;
        public static implicit operator OcsApiUrlPath(string o) => new(o);

        public override readonly string ToString()
        {
            return path;
        }
    }
}
