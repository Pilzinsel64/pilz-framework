﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Networking.CloudProviders.Nextcloud.Ocs
{
    public struct OcsApiAuthCredentials
    {
        public string LoginName { get; set; }
        public string AppPassword { get; set; }

        public OcsApiAuthCredentials(string loginName, string appPassword)
        {
            LoginName = loginName;
            AppPassword = appPassword;
        }
    }
}
