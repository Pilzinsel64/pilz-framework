﻿using Newtonsoft.Json;
using Pilz.Networking.CloudProviders.Nextcloud.Ocs.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Pilz.Networking.CloudProviders.Nextcloud.Ocs
{
    public class OcsApiResponse<TOcsResponse> where TOcsResponse : IOcsResponse
    {
        [JsonProperty("ocs")]
        public TOcsResponse? Ocs { get; set; }
    }
}
