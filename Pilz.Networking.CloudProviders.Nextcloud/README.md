# Nextcloud Client Api

This library is for interactive with Nextcloud instances.

**Beware:** This library is NOT complete. I usually expand it by the functions I need. So, feel free to extend it with your APIs and your functions and submit a Pull Request with the changes.

## How to use

This is a simple shot how the library is intended to be used.

*There are two ways to login. One is the LoginFlowV2.*

```cs
using NextcloudClient ncClient = new();
var userInfo = ncClient.Login(creds);

if (userInfo != null)
{
    var tableId = 16L;
    var tablesClient = ncClient.GetClient<TablesClient>();
    var rows = tablesClient.GetRows(tableId);
    if (rows != null)
    {
        foreach (var row in rows)
            Console.WriteLine(row.RowId);
    }
}
```