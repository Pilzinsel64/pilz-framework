﻿Namespace GeneralEventArgs

    Public Class GetValueEventArgs(Of T)
        Inherits EventArgs

        Public Property Value As T

        Public Sub New()
            MyBase.New
        End Sub

        Public Sub New(value As T)
            MyBase.New
        End Sub

    End Class

End Namespace
