﻿Imports System.IO
Imports System.Runtime.InteropServices

Namespace Runtime

    Public Module RuntimeInformationsEx

        Public ReadOnly Property OSType As OSType
            Get
                Static t As OSType? = Nothing

                If t Is Nothing Then
                    Select Case True
                        Case RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
                            t = OSType.Windows
                        Case RuntimeInformation.IsOSPlatform(OSPlatform.Linux)
                            t = OSType.Linux
                        Case RuntimeInformation.IsOSPlatform(OSPlatform.OSX)
                            t = OSType.OSX
                        Case Else
                            t = OSType.Unknown
                    End Select
                End If

                Return t
            End Get
        End Property

		Public ReadOnly Property RealOSType As OSType
			Get
				Static t As OSType? = Nothing

				If t Is Nothing Then

					Dim windir = Environment.GetEnvironmentVariable("windir")
					Const ostypeDirWine = "Z:\proc\sys\kernel\ostype"
					Const ostypeDirNative = "/proc/sys/kernel/ostype"
					Const systemVersionWine = "Z:\System\Library\CoreServices\SystemVersion.plist"
					Const systemVersionNative = "/System/Library/CoreServices/SystemVersion.plist"

					If File.Exists(ostypeDirWine) Then ' Linux using wine
						Dim osTypeString As String = File.ReadAllText(ostypeDirWine)
						If osTypeString.StartsWith("Linux", StringComparison.OrdinalIgnoreCase) Then
							' Note: Android gets here too
							t = OSType.Linux
						Else
							t = OSType.Unknown
						End If
					ElseIf File.Exists(ostypeDirNative) Then ' Linux native
						Dim osTypeString As String = File.ReadAllText(ostypeDirNative)
						If osTypeString.StartsWith("Linux", StringComparison.OrdinalIgnoreCase) Then
							' Note: Android gets here too
							t = OSType.Linux
						Else
							t = OSType.Unknown
						End If
					ElseIf File.Exists(systemVersionWine) Then ' OSX using wine
						' Note: iOS gets here too
						t = OSType.OSX
					ElseIf File.Exists(systemVersionNative) Then ' OSX native
						' Note: iOS gets here too
						t = OSType.OSX
					ElseIf Not String.IsNullOrEmpty(windir) AndAlso Directory.Exists(windir) AndAlso Path.DirectorySeparatorChar = "\"c Then ' Windows
						t = OSType.Windows
					Else
						t = OSType.Unknown
					End If

				End If

				Return t
			End Get
		End Property

		Public Function IsOSPlatform(os As OSType, checkRealOS As Boolean) As Boolean
			Return If(checkRealOS, RealOSType, OSType) = os
		End Function

	End Module

End Namespace
