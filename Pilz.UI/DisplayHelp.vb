﻿Imports System.Drawing
Imports System.Drawing.Drawing2D

Public Class DisplayHelp

    Public Shared Sub FillRectangle(ByVal g As Graphics, ByVal bounds As Rectangle, ByVal color1 As Color)
        FillRectangle(g, bounds, color1, Color.Empty, 90)
    End Sub

    Public Shared Sub FillRectangle(ByVal g As Graphics, ByVal bounds As Rectangle, ByVal color1 As Color, ByVal color2 As Color)
        FillRectangle(g, bounds, color1, color2, 90)
    End Sub

    Public Shared Sub FillRectangle(ByVal g As Graphics, ByVal r As Rectangle, ByVal color1 As Color, ByVal color2 As Color, ByVal gradientAngle As Integer)
        If r.Width = 0 OrElse r.Height = 0 Then Return

        If color2.IsEmpty Then

            If Not color1.IsEmpty Then
                Dim sm As SmoothingMode = g.SmoothingMode
                g.SmoothingMode = SmoothingMode.None

                Using brush As SolidBrush = New SolidBrush(color1)
                    g.FillRectangle(brush, r)
                End Using

                g.SmoothingMode = sm
            End If
        Else

            Using brush As LinearGradientBrush = CreateLinearGradientBrush(r, color1, color2, gradientAngle)
                g.FillRectangle(brush, r)
            End Using
        End If
    End Sub

    Public Shared Sub FillRectangle(ByVal g As Graphics, ByVal r As Rectangle, ByVal color1 As Color, ByVal color2 As Color, ByVal gradientAngle As Integer, ByVal factors As Single(), ByVal positions As Single())
        If r.Width = 0 OrElse r.Height = 0 Then Return

        If color2.IsEmpty Then

            If Not color1.IsEmpty Then
                Dim sm As SmoothingMode = g.SmoothingMode
                g.SmoothingMode = SmoothingMode.None

                Using brush As SolidBrush = New SolidBrush(color1)
                    g.FillRectangle(brush, r)
                End Using

                g.SmoothingMode = sm
            End If
        Else

            Using brush As LinearGradientBrush = CreateLinearGradientBrush(r, color1, color2, gradientAngle)
                Dim blend As Blend = New Blend(factors.Length)
                blend.Factors = factors
                blend.Positions = positions
                brush.Blend = blend
                g.FillRectangle(brush, r)
            End Using
        End If
    End Sub

    Public Shared Sub FillRoundedRectangle(ByVal g As Graphics, ByVal bounds As Rectangle, ByVal cornerSize As Integer, ByVal color1 As Color, ByVal color2 As Color, ByVal gradientAngle As Integer)
        If color2.IsEmpty Then

            If Not color1.IsEmpty Then

                Using brush As SolidBrush = New SolidBrush(color1)
                    FillRoundedRectangle(g, brush, bounds, cornerSize)
                End Using
            End If
        Else

            Using brush As LinearGradientBrush = CreateLinearGradientBrush(bounds, color1, color2, gradientAngle)
                FillRoundedRectangle(g, brush, bounds, cornerSize)
            End Using
        End If
    End Sub

    Public Shared Sub FillRoundedRectangle(ByVal g As Graphics, ByVal bounds As Rectangle, ByVal cornerSize As Integer, ByVal color1 As Color, ByVal color2 As Color)
        FillRoundedRectangle(g, bounds, cornerSize, color1, color2, 90)
    End Sub

    Public Shared Sub FillRoundedRectangle(ByVal g As Graphics, ByVal bounds As Rectangle, ByVal cornerSize As Integer, ByVal color1 As Color)
        Using brush As SolidBrush = New SolidBrush(color1)
            FillRoundedRectangle(g, brush, bounds, cornerSize)
        End Using
    End Sub

    Public Shared Sub FillRoundedRectangle(ByVal g As Graphics, ByVal brush As Brush, ByVal bounds As Rectangle, ByVal cornerSize As Integer)
        If cornerSize <= 0 Then
            Dim sm As SmoothingMode = g.SmoothingMode
            g.SmoothingMode = SmoothingMode.None
            g.FillRectangle(brush, bounds)
            g.SmoothingMode = sm
        Else
            bounds.Width -= 1
            bounds.Height -= 1

            Using path As GraphicsPath = GetRoundedRectanglePath(bounds, cornerSize)
                g.FillPath(brush, path)
            End Using
        End If
    End Sub

    Public Shared Sub DrawRectangle(ByVal g As System.Drawing.Graphics, ByVal color As Color, ByVal x As Integer, ByVal y As Integer, ByVal width As Integer, ByVal height As Integer)
        Using pen As Pen = New Pen(color, 1)
            DrawRectangle(g, pen, x, y, width, height)
        End Using
    End Sub

    Public Shared Sub DrawRectangle(ByVal g As System.Drawing.Graphics, ByVal color As Color, ByVal r As System.Drawing.Rectangle)
        DrawRectangle(g, color, r.X, r.Y, r.Width, r.Height)
    End Sub

    Public Shared Sub DrawRectangle(ByVal g As System.Drawing.Graphics, ByVal pen As System.Drawing.Pen, ByVal x As Integer, ByVal y As Integer, ByVal width As Integer, ByVal height As Integer)
        width -= 1
        height -= 1
        g.DrawRectangle(pen, x, y, width, height)
    End Sub

    Public Shared Sub DrawRoundedRectangle(ByVal g As System.Drawing.Graphics, ByVal color As Color, ByVal bounds As Rectangle, ByVal cornerSize As Integer)
        If Not color.IsEmpty Then

            Using pen As Pen = New Pen(color)
                DrawRoundedRectangle(g, pen, bounds.X, bounds.Y, bounds.Width, bounds.Height, cornerSize)
            End Using
        End If
    End Sub

    Public Shared Sub DrawRoundedRectangle(ByVal g As System.Drawing.Graphics, ByVal pen As System.Drawing.Pen, ByVal x As Integer, ByVal y As Integer, ByVal width As Integer, ByVal height As Integer, ByVal cornerSize As Integer)
        DrawRoundedRectangle(g, pen, Nothing, x, y, width, height, cornerSize)
    End Sub

    Public Shared Sub DrawRoundedRectangle(ByVal g As System.Drawing.Graphics, ByVal pen As System.Drawing.Pen, ByVal fill As Brush, ByVal x As Integer, ByVal y As Integer, ByVal width As Integer, ByVal height As Integer, ByVal cornerSize As Integer)
        width -= 1
        height -= 1
        Dim r As Rectangle = New Rectangle(x, y, width, height)

        Using path As GraphicsPath = GetRoundedRectanglePath(r, cornerSize)
            If fill IsNot Nothing Then g.FillPath(fill, path)
            g.DrawPath(pen, path)
        End Using
    End Sub

    Public Shared Function GetRoundedRectanglePath(ByVal r As Rectangle, ByVal cornerSize As Integer) As GraphicsPath
        Dim path As GraphicsPath = New GraphicsPath()

        If cornerSize = 0 Then
            path.AddRectangle(r)
        Else
            AddCornerArc(path, r, cornerSize, eCornerArc.TopLeft)
            AddCornerArc(path, r, cornerSize, eCornerArc.TopRight)
            AddCornerArc(path, r, cornerSize, eCornerArc.BottomRight)
            AddCornerArc(path, r, cornerSize, eCornerArc.BottomLeft)
            path.CloseAllFigures()
        End If

        Return path
    End Function

    Public Shared Function CreateLinearGradientBrush(ByVal r As Rectangle, ByVal color1 As Color, ByVal color2 As Color, ByVal gradientAngle As Single) As LinearGradientBrush
        If r.Width <= 0 Then r.Width = 1
        If r.Height <= 0 Then r.Height = 1
        Return New LinearGradientBrush(New Rectangle(r.X, r.Y - 1, r.Width, r.Height + 1), color1, color2, gradientAngle)
    End Function

    Public Shared Sub AddCornerArc(ByVal path As GraphicsPath, ByVal bounds As Rectangle, ByVal cornerDiameter As Integer, ByVal corner As eCornerArc)
        If cornerDiameter > 0 Then
            Dim a As ArcData = GetCornerArc(bounds, cornerDiameter, corner)
            path.AddArc(a.X, a.Y, a.Width, a.Height, a.StartAngle, a.SweepAngle)
        Else

            If corner = eCornerArc.TopLeft Then
                path.AddLine(bounds.X, bounds.Y + 2, bounds.X, bounds.Y)
            ElseIf corner = eCornerArc.BottomLeft Then
                path.AddLine(bounds.X + 2, bounds.Bottom, bounds.X, bounds.Bottom)
            ElseIf corner = eCornerArc.TopRight Then
                path.AddLine(bounds.Right - 2, bounds.Y, bounds.Right, bounds.Y)
            ElseIf corner = eCornerArc.BottomRight Then
                path.AddLine(bounds.Right, bounds.Bottom - 2, bounds.Right, bounds.Bottom)
            End If
        End If
    End Sub

    Friend Shared Function GetCornerArc(ByVal bounds As Rectangle, ByVal cornerDiameter As Integer, ByVal corner As eCornerArc) As ArcData
        Dim a As ArcData
        If cornerDiameter = 0 Then cornerDiameter = 1
        Dim diameter As Integer = cornerDiameter * 2

        Select Case corner
            Case eCornerArc.TopLeft
                a = New ArcData(bounds.X, bounds.Y, diameter, diameter, 180, 90)
            Case eCornerArc.TopRight
                a = New ArcData(bounds.Right - diameter, bounds.Y, diameter, diameter, 270, 90)
            Case eCornerArc.BottomLeft
                a = New ArcData(bounds.X, bounds.Bottom - diameter, diameter, diameter, 90, 90)
            Case Else
                a = New ArcData(bounds.Right - diameter, bounds.Bottom - diameter, diameter, diameter, 0, 90)
        End Select

        Return a
    End Function

    Public Enum eCornerArc
        TopLeft
        TopRight
        BottomLeft
        BottomRight
    End Enum


    Friend Structure ArcData
        Public X As Integer
        Public Y As Integer
        Public Width As Integer
        Public Height As Integer
        Public StartAngle As Single
        Public SweepAngle As Single

        Public Sub New(ByVal x As Integer, ByVal y As Integer, ByVal width As Integer, ByVal height As Integer, ByVal startAngle As Single, ByVal sweepAngle As Single)
            Me.X = x
            Me.Y = y
            Me.Width = width
            Me.Height = height
            Me.StartAngle = startAngle
            Me.SweepAngle = sweepAngle
        End Sub
    End Structure

End Class
