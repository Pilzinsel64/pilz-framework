﻿Imports System.Windows.Forms

Public Module HelpfulFunctions

    Public Sub Sleep(milliseconds As Integer)
        Dim stopw As New Stopwatch

        stopw.Start()

        Do While stopw.ElapsedMilliseconds < milliseconds
            Application.DoEvents()
        Loop

        stopw.Stop()
    End Sub

End Module
