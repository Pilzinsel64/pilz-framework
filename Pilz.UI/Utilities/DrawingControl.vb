Imports System.Runtime.CompilerServices
Imports System.Windows.Forms

Namespace Utils

    Public Module DrawingControl

        Private Const WM_SETREDRAW = 11
        Private ReadOnly dicSuspendCount As New Dictionary(Of IntPtr, Integer)

        <Extension>
        Public Sub SuspendDrawing(control As Control)
            If Not dicSuspendCount.ContainsKey(control.Handle) Then
                dicSuspendCount.Add(control.Handle, 1)
            Else
                dicSuspendCount(control.Handle) += 1
            End If

            If dicSuspendCount(control.Handle) = 1 Then
                SendMessage(control.Handle, WM_SETREDRAW, False, 0)
            End If
        End Sub

        <Extension>
        Public Sub ResumeDrawing(control As Control)
            ResumeDrawing(control, True)
        End Sub

        <Extension>
        Public Sub ResumeDrawing(control As Control, redraw As Boolean)
            Dim doRedraw As Boolean = True

            If dicSuspendCount.ContainsKey(control.Handle) Then
                dicSuspendCount(control.Handle) -= 1
                If dicSuspendCount(control.Handle) >= 1 Then
                    doRedraw = False
                End If
            End If

            If doRedraw Then
                SendMessage(control.Handle, WM_SETREDRAW, True, 0)
                If redraw Then control.Refresh()
            End If
        End Sub

    End Module

End Namespace
