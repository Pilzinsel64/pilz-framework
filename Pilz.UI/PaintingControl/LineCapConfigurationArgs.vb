﻿Imports System.Drawing.Drawing2D

Public Class LineCapConfigurationArgs

    Public ReadOnly Property LineCap As LineCap
    Public ReadOnly Property CustomLineCap As CustomLineCap

    Public Sub New(lineCap As LineCap)
        Me.New(lineCap, Nothing)
    End Sub

    Public Sub New(customLineCap As CustomLineCap)
        Me.New(Nothing, customLineCap)
    End Sub

    Public Sub New(lineCap As LineCap, customLineCap As CustomLineCap)
        Me.LineCap = lineCap
        Me.CustomLineCap = customLineCap
    End Sub

End Class
