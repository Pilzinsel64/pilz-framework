﻿Imports System.Drawing
Imports System.IO
Imports System.Windows.Forms
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class PaintingObjectEventArgs
    Inherits EventArgs

    Public ReadOnly Property PaintingObjects As PaintingObject() = Nothing

    Friend Sub New(paintingObjects As PaintingObject())
        _PaintingObjects = paintingObjects
    End Sub

End Class
