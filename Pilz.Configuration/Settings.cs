﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Pilz.Configuration
{
    public class Settings : ISettings
    {
        [JsonProperty(nameof(Settings))]
        protected readonly Dictionary<string, IChildSettings> mySettings = [];

        [JsonIgnore]
        public IReadOnlyCollection<IChildSettings> Childs => mySettings.Values;

        public T Get<T>() where T : IChildSettings, ISettingsIdentifier
        {
            if (mySettings.TryGetValue(T.Identifier, out IChildSettings valueExisting) && valueExisting is T settingsExisting)
                return settingsExisting;

            if (Activator.CreateInstance<T>() is T settingsNew)
            {
                settingsNew.Reset();
                mySettings.Add(T.Identifier, settingsNew);
                return settingsNew;
            }

            return default;
        }

        public void Reset()
        {
            foreach (var s in mySettings.Values)
                s.Reset();
        }
    }
}