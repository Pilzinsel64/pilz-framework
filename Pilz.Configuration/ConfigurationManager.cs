﻿namespace Pilz.Configuration
{
    public abstract class ConfigurationManager
    {
        public SimpleConfiguration Configuration { get; private set; }

        internal void SetConfiguration(SimpleConfiguration configuration)
        {
            Configuration = configuration;
        }
    }
}