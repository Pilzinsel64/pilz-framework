﻿using System.Collections.Generic;

namespace Pilz.Configuration
{
    public interface ISettings
    {
        IReadOnlyCollection<IChildSettings> Childs { get; }
        T Get<T>() where T : IChildSettings, ISettingsIdentifier;
        void Reset();
    }
}