﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Configuration
{
    public interface ISettingsIdentifier
    {
        static abstract string Identifier { get; }
    }
}
