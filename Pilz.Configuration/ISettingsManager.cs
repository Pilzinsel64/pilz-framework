﻿
namespace Pilz.Configuration
{
    public interface ISettingsManager
    {
        ISettings Instance { get; }
        void Save();
        void Load();
        void Reset();
    }
}