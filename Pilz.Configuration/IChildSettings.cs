﻿namespace Pilz.Configuration
{
    public interface IChildSettings
    {
        void Reset();
    }
}