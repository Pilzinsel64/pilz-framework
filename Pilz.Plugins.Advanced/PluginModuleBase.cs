﻿using System.ComponentModel;

namespace Pilz.Plugins.Advanced
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public abstract class PluginModuleBase : PluginFeature
    {
        protected PluginModuleBase(string moduleType, string moduleIdentifier) : base(moduleType, moduleIdentifier)
        {
        }

        protected PluginModuleBase(string moduleType, string moduleIdentifier, string moduleName) : base(moduleType, moduleIdentifier, moduleName)
        {
        }
    }
}