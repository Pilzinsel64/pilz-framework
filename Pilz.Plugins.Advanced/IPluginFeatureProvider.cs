﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Plugins.Advanced
{
    public interface IPluginFeatureProvider
    {
        static abstract PluginFeature Instance { get; }
    }

    public interface IPluginFeatureProvider<T> : IPluginFeatureProvider where T : PluginFeature, IPluginFeatureProvider<T>
    {
        static new abstract T Instance { get; }
        static PluginFeature IPluginFeatureProvider.Instance => T.Instance;
    }
}
