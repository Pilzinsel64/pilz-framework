﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Plugins.Advanced
{
    public sealed class PluginFunctionSimpleParamter : PluginFunctionParameter
    {
        public object?[]? Params { get; init; }

        public PluginFunctionSimpleParamter(params object?[]? @params)
        {
            Params = @params;
        }
    }
}
