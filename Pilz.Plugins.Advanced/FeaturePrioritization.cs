﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Plugins.Advanced
{
    public enum FeaturePrioritization
    {
        Low = -1,
        Default = 0,
        High = 1,
    }
}
