﻿using System.Security.Cryptography;
using System.Text;

namespace Pilz.Gaming.Minecraft
{
    public static class Utils
    {
        public static string GetUUID(string value)
        {
            using var md5 = MD5.Create();

            //extracted from the java code:
            //new GameProfile(UUID.nameUUIDFromBytes(("OfflinePlayer:" + name).getBytes(Charsets.UTF_8)), name));
            byte[] data = md5.ComputeHash(Encoding.ASCII.GetBytes(value));
            //set the version to 3 -> Name based md5 hash
            data[6] = Convert.ToByte(data[6] & 0x0f | 0x30);
            //IETF variant
            data[8] = Convert.ToByte(data[8] & 0x3f | 0x80);

            //example: 069a79f4-44e9-4726-a5be-fca90e38aaf5
            var striped = Convert.ToHexString(data);
            var components = new string[] {
                striped[..].Remove(8),
                striped[8..].Remove(4),
                striped[12..].Remove(4),
                striped[16..].Remove(4),
                striped[20..]
            };

            return string.Join('-', components).ToLower();
        }

        public static string GetPlayerUUID(string username, bool offlineMode)
        {
            using var md5 = MD5.Create();

            if (!offlineMode)
                throw new NotSupportedException("Getting player's online UUID via the Mojang API is not supported at this time.");

            return GetUUID("OfflinePlayer:" + username);
        }
    }
}
