﻿Namespace SimpleHistory

    ''' <summary>
    ''' List contianing member names to include.
    ''' </summary>
    Public Class MemberWhiteList
        Inherits List(Of String)

        Public Sub New()
            MyBase.New
        End Sub

        Public Sub New(entries As String())
            MyBase.New(entries)
        End Sub
    End Class

    ''' <summary>
    ''' List contianing member names to exclude
    ''' </summary>
    Public Class MemberBlackList
        Inherits List(Of String)

        Public Sub New()
            MyBase.New
        End Sub

        Public Sub New(entries As String())
            MyBase.New(entries)
        End Sub
    End Class

End Namespace