﻿Imports System.Drawing
Imports System.Runtime.CompilerServices

Public Module BitmapExtensions

    <Extension>
    Public Sub InvertColors(bmp As Bitmap)
        For y As Integer = 0 To bmp.Height - 1
            For x As Integer = 0 To bmp.Width - 1
                Dim inv As Color = bmp.GetPixel(x, y)
                inv = Color.FromArgb(inv.A,
                                     Byte.MaxValue - inv.R,
                                     Byte.MaxValue - inv.G,
                                     Byte.MaxValue - inv.B)
                bmp.SetPixel(x, y, inv)
            Next
        Next
    End Sub

End Module
