﻿Imports System.Drawing
Imports System.Drawing.Drawing2D

Public Module HelpfulDrawingFunctions

    Public Function IsPointInRectangle(p As PointF, rect As RectangleF) As Boolean
        Dim bList As New List(Of Boolean)

        bList.Add(p.X > rect.Left)
        bList.Add(p.X < rect.Right)
        bList.Add(p.Y > rect.Top)
        bList.Add(p.Y < rect.Bottom)

        Return Not bList.Contains(False)
    End Function

    Public Function OverlapsTwoRectangles(a As RectangleF, b As RectangleF) As Boolean
        Return a.IntersectsWith(b) 'RectangleF.Intersect(a, b) <> RectangleF.Empty
    End Function

    Public Function RectangleContainsRectangle(parent As RectangleF, child As RectangleF) As Boolean
        Return parent.Contains(child)
        'Return _
        '    IsPointInRectangle(New PointF(child.Top, child.Left), parent) AndAlso
        '    IsPointInRectangle(New PointF(child.Top, child.Right), parent) AndAlso
        '    IsPointInRectangle(New PointF(child.Bottom, child.Left), parent) AndAlso
        '    IsPointInRectangle(New PointF(child.Bottom, child.Right), parent)
    End Function

    Public Function GetRectangle(p1 As PointF, p2 As PointF) As RectangleF
        Dim rect As New RectangleF
        Dim startIsEnd As Boolean = p1.X > p2.X AndAlso p1.Y > p2.Y

        Dim xValues() As Integer = {p1.X, p2.X}
        Dim yValues() As Integer = {p1.Y, p2.Y}

        rect.Location = New PointF(xValues.OrderBy(Function(n) n).First,
                                  yValues.OrderBy(Function(n) n).First)

        rect.Size = New SizeF(xValues.OrderByDescending(Function(n) n).First,
                             yValues.OrderByDescending(Function(n) n).First)

        rect.Size = New SizeF(rect.Size.Width - rect.Location.X,
                             rect.Size.Height - rect.Location.Y)

        Return rect
    End Function

End Module

