﻿using System.Drawing;
using System.Reflection;
using Telerik.WinControls;
using Telerik.WinControls.Svg;

namespace Pilz.UI.Telerik
{
    public abstract class SymbolFactory<TSvgSymbols> where TSvgSymbols : Enum
    {
        public abstract string GetSvgImageRessourcePath(TSvgSymbols svgImage);
        public abstract Assembly GetSvgImageResourceAssembly();

        protected virtual Size ResolveCommonSize(SvgImageSize size)
        {
            return size switch
            {
                SvgImageSize.Small => new Size(16, 16),
                SvgImageSize.Medium => new Size(20, 20),
                SvgImageSize.Large => new Size(32, 32),
                _ => Size.Empty,
            };
        }

        public virtual Stream? GetSvgImageRessourceStream(TSvgSymbols svgImage)
        {
            var asm = GetSvgImageResourceAssembly();
            var path = GetSvgImageRessourcePath(svgImage);
            return asm.GetManifestResourceStream(path);
        }

        public virtual RadSvgImage GetSvgImage(TSvgSymbols svgImage, SvgImageSize size)
        {
            return GetSvgImage(svgImage, ResolveCommonSize(size));
        }

        public virtual RadSvgImage GetSvgImage(TSvgSymbols svgImage, Size size)
        {
            using var stream = GetSvgImageRessourceStream(svgImage);
            var img = RadSvgImage.FromStream(stream);

            if (!size.IsEmpty)
                img.Size = size;

            return img;
        }

        public virtual RadSvgImage GetSvgImageColored(TSvgSymbols svgImage, SvgImageSize size, Color color)
        {
            return GetSvgImageColored(svgImage, ResolveCommonSize(size), color);
        }

        public virtual RadSvgImage GetSvgImageColored(TSvgSymbols svgImage, Size size, Color color)
        {
            var img = GetSvgImage(svgImage, size);
            img.ApplyColor(color);
            return img;
        }

        public virtual Image GetImage(TSvgSymbols svgImage, SvgImageSize size)
        {
            return GetImage(svgImage, ResolveCommonSize(size));
        }

        public virtual Image GetImage(TSvgSymbols svgImage, Size size)
        {
            return GetImageFromSvg(GetSvgImage(svgImage, size));
        }

        public virtual Image GetImageColored(TSvgSymbols svgImage, SvgImageSize size, Color color)
        {
            return GetImageColored(svgImage, ResolveCommonSize(size), color);
        }

        public virtual Image GetImageColored(TSvgSymbols svgImage, Size size, Color color)
        {
            return GetImageFromSvg(GetSvgImageColored(svgImage, size, color));
        }

        public virtual Image GetImageFromSvg(RadSvgImage svg)
        {
            return svg.ToImage();
        }
    }
}