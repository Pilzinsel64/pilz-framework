﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls;
using Telerik.WinControls.Svg;

namespace Pilz.UI.Telerik
{
    public static class Extensions
    {
        public static Image ToImage(this RadSvgImage svg)
        {
            return svg?.Document.Draw(svg.Width, svg.Height);
        }
        
        public static void ApplyColor(this RadSvgImage svg, Color color)
        {
            svg.Document.Fill = new SvgColourServer(color);
            svg.ClearCache();
        }
    }
}
