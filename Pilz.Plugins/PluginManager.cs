﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Plugins
{
    public class PluginManager : PluginManager<IPlugin, PluginRuntimeInfo>
    {
        public static PluginManager Instance { get; private set; } = new();
    }
}
