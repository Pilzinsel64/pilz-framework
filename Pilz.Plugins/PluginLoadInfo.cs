﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Plugins
{
    public class PluginLoadInfo<TPluginInterface, TPluginRuntimeInfo> where TPluginInterface : class where TPluginRuntimeInfo : PluginRuntimeInfo<TPluginInterface>
    {
        internal List<TPluginRuntimeInfo> PluginsInternal { get; } = [];
        public Assembly Assembly { get; internal set; }
        public PluginLoadStatus Status { get; internal set;  }
        public IEnumerable<TPluginRuntimeInfo> Plugins => PluginsInternal.AsReadOnly();
    }
}
