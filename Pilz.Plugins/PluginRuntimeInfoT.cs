﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Plugins
{
    public class PluginRuntimeInfo<T> where T : class
    {
        public T? Plugin { get; internal set; }
        public PluginStatus Status { get; internal set; }
        public Assembly? Assembly { get; internal set; }
    }
}
