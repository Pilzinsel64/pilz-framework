﻿Namespace Native

    <Flags>
    Public Enum FileInfoFlags As Integer
        ''' <summary>
        ''' Retrieve the handle to the icon that represents the file and the index 
        ''' of the icon within the system image list. The handle is copied to the 
        ''' hIcon member of the structure specified by psfi, and the index is copied 
        ''' to the iIcon member.
        ''' </summary>
        SHGFI_ICON = &H100
        ''' <summary>
        ''' Indicates that the function should not attempt to access the file 
        ''' specified by pszPath. Rather, it should act as if the file specified by 
        ''' pszPath exists with the file attributes passed in dwFileAttributes.
        ''' </summary>
        SHGFI_USEFILEATTRIBUTES = &H10
    End Enum

End Namespace