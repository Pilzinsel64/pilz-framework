﻿Imports System.Drawing
Imports System.Runtime.InteropServices

Namespace Native

    <Serializable, StructLayout(LayoutKind.Sequential)>
    Public Structure RECT
        Public Left As Integer
        Public Top As Integer
        Public Right As Integer
        Public Bottom As Integer

        Public Sub New(ByVal left_ As Integer, ByVal top_ As Integer, ByVal right_ As Integer, ByVal bottom_ As Integer)
            Left = left_
            Top = top_
            Right = right_
            Bottom = bottom_
        End Sub

        Public Sub New(ByVal r As Rectangle)
            Left = r.Left
            Top = r.Top
            Right = r.Right
            Bottom = r.Bottom
        End Sub

        Public ReadOnly Property Height As Integer
            Get
                Return Bottom - Top
            End Get
        End Property

        Public ReadOnly Property Width As Integer
            Get
                Return Right - Left
            End Get
        End Property

        Public ReadOnly Property Size As System.Drawing.Size
            Get
                Return New System.Drawing.Size(Width, Height)
            End Get
        End Property

        Public ReadOnly Property Location As System.Drawing.Point
            Get
                Return New System.Drawing.Point(Left, Top)
            End Get
        End Property

        Public Function ToRectangle() As Rectangle
            Return Rectangle.FromLTRB(Left, Top, Right, Bottom)
        End Function

        Public Shared Function FromRectangle(ByVal rectangle As Rectangle) As RECT
            Return New RECT(rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Bottom)
        End Function

        Public Overrides Function GetHashCode() As Integer
            Return Left ^ ((Top << 13) Or (Top >> &H13)) _
                ^ ((Width << &H1A) Or (Width >> 6)) _
                ^ ((Height << 7) Or (Height >> &H19))
        End Function

        Public Shared Function FromXYWH(ByVal x As Integer, ByVal y As Integer, ByVal width As Integer, ByVal height As Integer) As RECT
            Return New RECT(x, y, x + width, y + height)
        End Function

        Public Shared Widening Operator CType(ByVal rect As RECT) As Rectangle
            Return Rectangle.FromLTRB(rect.Left, rect.Top, rect.Right, rect.Bottom)
        End Operator

        Public Shared Widening Operator CType(ByVal rect As Rectangle) As RECT
            Return New RECT(rect.Left, rect.Top, rect.Right, rect.Bottom)
        End Operator

        Public Overrides Function ToString() As String
            Return "Left=" & Me.Left & ", Top=" & Me.Top & ", Right=" & Me.Right & ", Bottom=" & Me.Bottom
        End Function
    End Structure

End Namespace
