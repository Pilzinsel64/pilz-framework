﻿Namespace Native

    <Flags>
    Public Enum WindowFromPointFlags
        CWP_ALL = &H0
        CWP_SKIPINVISIBLE = &H1
        CWP_SKIPDISABLED = &H2
        CWP_SKIPTRANSPARENT = &H4
    End Enum

End Namespace