﻿Imports System.Runtime.InteropServices

Namespace Native

    Public Class User32

        Private Const LIB_USER32 As String = "user32.dll"

        <DllImport(LIB_USER32)>
        Public Shared Function GetWindowRect(ByVal hWnd As IntPtr, ByRef r As RECT) As Boolean
        End Function

        <DllImport(LIB_USER32)>
        Public Shared Function ChildWindowFromPointEx(ByVal hWndParent As IntPtr, ByVal pt As POINT, ByVal uFlags As UInteger) As IntPtr
        End Function

        <DllImport(LIB_USER32)>
        Public Shared Function GetKeyState(keyCode As Integer) As Short
        End Function

    End Class

End Namespace
