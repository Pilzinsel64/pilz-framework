﻿Imports System.Runtime.InteropServices

Namespace Native

    <StructLayout(LayoutKind.Sequential)>
    Public Structure POINT
        Public Sub New(ByVal p As System.Drawing.Point)
            Me.x = p.X
            Me.y = p.Y
        End Sub

        Public Sub New(ByVal x As Integer, ByVal y As Integer)
            Me.x = x
            Me.y = y
        End Sub

        Public x As Integer
        Public y As Integer
    End Structure

End Namespace
