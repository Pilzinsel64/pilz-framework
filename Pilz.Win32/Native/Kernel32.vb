﻿Imports System.Runtime.InteropServices
Imports System.Text

Namespace Native

    Public Class Kernel32

        Private Const LIB_KERNEL32 As String = "kernel32.dll"

        <DllImport(LIB_KERNEL32)>
        Public Shared Function GetModuleFileName(hModule As IntPtr, lpFilename As StringBuilder, nSize As Integer) As UInteger
        End Function

    End Class

End Namespace
