﻿Imports System.Runtime.InteropServices

Namespace Native

    ''' <summary>
    ''' Contains information about a file object. 
    ''' </summary>
    <StructLayout(LayoutKind.Sequential)>
    Public Structure SHFILEINFO
        Public Const SHGFI_ICON As UInteger = &H100
        Public Const SHGFI_LARGEICON As UInteger = &H0
        Public Const SHGFI_SMALLICON As UInteger = &H1
        ''' <summary>
        ''' Handle to the icon that represents the file. You are responsible for
        ''' destroying this handle with DestroyIcon when you no longer need it. 
        ''' </summary>
        Public hIcon As IntPtr

        ''' <summary>
        ''' Index of the icon image within the system image list.
        ''' </summary>
        Public iIcon As IntPtr

        ''' <summary>
        ''' Array of values that indicates the attributes of the file object.
        ''' For information about these values, see the IShellFolder::GetAttributesOf
        ''' method.
        ''' </summary>
        Public dwAttributes As UInteger

        ''' <summary>
        ''' String that contains the name of the file as it appears in the Microsoft
        ''' Windows Shell, or the path and file name of the file that contains the
        ''' icon representing the file.
        ''' </summary>
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=260)>
        Public szDisplayName As String

        ''' <summary>
        ''' String that describes the type of file.
        ''' </summary>
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=80)>
        Public szTypeName As String
    End Structure

End Namespace
