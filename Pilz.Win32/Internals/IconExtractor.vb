﻿Imports System.Drawing
Imports System.Runtime.InteropServices

Imports Pilz.Win32.Mapped
Imports Pilz.Win32.Native

Namespace Internals

    Public Class IconExtractor

        ''' <summary>
        ''' Extrahiert das Icon aus einer Datei oder aus einem Ordner.
        ''' </summary>
        ''' <param name="FilePath">Hier übergeben Sie den Pfad der Datei von dem das Icon extrahiert werden soll.</param>
        ''' <param name="size">Bei übergabe von true wird ein kleines und bei false ein großes Icon zurück gegeben.</param>
        Public Shared Function ExtractIcon(FilePath As String, size As SystemIconSize) As Icon
            Dim icon As Icon
            Dim shinfo As New SHFILEINFO
            Dim small As Boolean = size = SystemIconSize.Small

            Shell32.SHGetFileInfo(FilePath, 0, shinfo, Math.Truncate(Marshal.SizeOf(shinfo)), SHFILEINFO.SHGFI_ICON Or If(small, SHFILEINFO.SHGFI_SMALLICON, SHFILEINFO.SHGFI_LARGEICON))

            Try
                icon = Icon.FromHandle(shinfo.hIcon)
            Catch ex As Exception
                icon = Nothing
            End Try

            Return icon
        End Function

    End Class

End Namespace
