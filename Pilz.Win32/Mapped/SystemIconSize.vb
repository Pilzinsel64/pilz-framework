﻿Namespace Mapped

    ''' <summary>
    ''' Two constants extracted from the FileInfoFlags, the only that are
    ''' meaningfull for the user of this class.
    ''' </summary>
    Public Enum SystemIconSize As Integer
        Large
        Small
    End Enum

End Namespace
