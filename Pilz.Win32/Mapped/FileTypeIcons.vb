﻿Imports System.Drawing

Namespace Mapped

    Public Module FileTypeIcons

        Public Function ExtractIconFromFilePath(filePath As String, size As SystemIconSize) As Icon
            Return Internals.IconExtractor.ExtractIcon(filePath, size)
        End Function

        Public Function ExtractIconFromFileExtension(fileExtension As String, size As SystemIconSize) As Icon
            Return Internals.IconFactory.IconFromExtensionShell(fileExtension, size)
        End Function

    End Module

End Namespace
