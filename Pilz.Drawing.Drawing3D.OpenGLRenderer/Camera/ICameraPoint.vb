﻿Namespace CameraN

    Public Interface ICameraPoint

        Property Position As Numerics.Vector3
        Property Rotation As Numerics.Vector3

    End Interface

End Namespace
