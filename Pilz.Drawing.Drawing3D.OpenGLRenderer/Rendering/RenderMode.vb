﻿Namespace RenderingN

    Public Enum RenderMode As Byte
        None = &H0
        Fill = &H1
        Outline = &H2
        FillOutline = Fill Or Outline
    End Enum

End Namespace
