﻿using System.Drawing;
using System.Windows.Forms;

namespace Pilz.Plugins.Advanced.UI
{
    public abstract class PluginModule<TPluginModuleUI> : PluginModuleBase where TPluginModuleUI : Control
    {
        public delegate void PluginModuleUIEventHandler(PluginModuleBase module, TPluginModuleUI ui);

        /// <summary>
        /// Fires when a <see cref="PluginModuleUI"/> instance has been created.
        /// </summary>
        public static event PluginModuleUIEventHandler? OnUICreated;

        public bool Visible { get; set; } = true;
        public bool AllowEmbedding { get; set; } = true;

        /// <summary>
        /// Wrapper for the <see cref="PluginFeature.Icon"/> property to directly use it as <see cref="System.Drawing.Image"/>.
        /// </summary>
        public Image? Image
        {
            get => base.Icon as Image;
            set => base.Icon = value;
        }

        protected PluginModule(string moduleType, string moduleIdentifier) : base(moduleType, moduleIdentifier)
        {
        }

        protected PluginModule(string moduleType, string moduleIdentifier, string moduleName) : base(moduleType, moduleIdentifier, moduleName)
        {
        }

        public virtual void ShowUI()
        {
        }

        public virtual TPluginModuleUI CreateUI()
        {
            var ui = CreateNewUI();
            OnUICreated?.Invoke(this, ui);
            return ui;
        }

        protected abstract TPluginModuleUI CreateNewUI();
    }
}