﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pilz.Cryptography
{
    [JsonConverter(typeof(Json.Converters.UniquieIDStringJsonConverter))]
    public interface IUniquieID
    {
        bool HasID { get; }
        string ID { get; }

        void GenerateIfNull();
        void Generate();
        bool Equals(object obj);
    }
}
