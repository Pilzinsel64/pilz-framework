﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pilz.Cryptography
{
    /// <summary>
    /// Can be implemented on objects that provides an UniquieID.
    /// </summary>
    public interface IUniquieIDHost
    {
        UniquieID ID { get; }
    }
}
