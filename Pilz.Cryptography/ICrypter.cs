﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilz.Cryptography
{
    public interface ICrypter
    {
        string Encrypt(string plainValue);
        string Decrypt(string encryptedValue);
    }
}
