﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pilz.Cryptography
{
    public enum UniquieIDGenerationMode
    {
        None,
        GenerateOnGet,
        GenerateOnInit
    }
}
